-- Automatically generated packer.nvim plugin loader code

if vim.api.nvim_call_function('has', {'nvim-0.5'}) ~= 1 then
  vim.api.nvim_command('echohl WarningMsg | echom "Invalid Neovim version for packer.nvim! | echohl None"')
  return
end

vim.api.nvim_command('packadd packer.nvim')

local no_errors, error_msg = pcall(function()

  local time
  local profile_info
  local should_profile = false
  if should_profile then
    local hrtime = vim.loop.hrtime
    profile_info = {}
    time = function(chunk, start)
      if start then
        profile_info[chunk] = hrtime()
      else
        profile_info[chunk] = (hrtime() - profile_info[chunk]) / 1e6
      end
    end
  else
    time = function(chunk, start) end
  end
  
local function save_profiles(threshold)
  local sorted_times = {}
  for chunk_name, time_taken in pairs(profile_info) do
    sorted_times[#sorted_times + 1] = {chunk_name, time_taken}
  end
  table.sort(sorted_times, function(a, b) return a[2] > b[2] end)
  local results = {}
  for i, elem in ipairs(sorted_times) do
    if not threshold or threshold and elem[2] > threshold then
      results[i] = elem[1] .. ' took ' .. elem[2] .. 'ms'
    end
  end

  _G._packer = _G._packer or {}
  _G._packer.profile_output = results
end

time([[Luarocks path setup]], true)
local package_path_str = "/home/anindra/.cache/nvim/packer_hererocks/2.1.0-beta3/share/lua/5.1/?.lua;/home/anindra/.cache/nvim/packer_hererocks/2.1.0-beta3/share/lua/5.1/?/init.lua;/home/anindra/.cache/nvim/packer_hererocks/2.1.0-beta3/lib/luarocks/rocks-5.1/?.lua;/home/anindra/.cache/nvim/packer_hererocks/2.1.0-beta3/lib/luarocks/rocks-5.1/?/init.lua"
local install_cpath_pattern = "/home/anindra/.cache/nvim/packer_hererocks/2.1.0-beta3/lib/lua/5.1/?.so"
if not string.find(package.path, package_path_str, 1, true) then
  package.path = package.path .. ';' .. package_path_str
end

if not string.find(package.cpath, install_cpath_pattern, 1, true) then
  package.cpath = package.cpath .. ';' .. install_cpath_pattern
end

time([[Luarocks path setup]], false)
time([[try_loadstring definition]], true)
local function try_loadstring(s, component, name)
  local success, result = pcall(loadstring(s), name, _G.packer_plugins[name])
  if not success then
    vim.schedule(function()
      vim.api.nvim_notify('packer.nvim: Error running ' .. component .. ' for ' .. name .. ': ' .. result, vim.log.levels.ERROR, {})
    end)
  end
  return result
end

time([[try_loadstring definition]], false)
time([[Defining packer_plugins]], true)
_G.packer_plugins = {
  ["bufferline.nvim"] = {
    loaded = true,
    path = "/home/anindra/.local/share/nvim/site/pack/packer/start/bufferline.nvim",
    url = "https://github.com/akinsho/bufferline.nvim"
  },
  ["neo-tree.nvim"] = {
    config = { "\27LJ\2\2�\t\0\0\5\0\23\0\0316\0\0\0'\1\1\0B\0\2\0029\0\2\0005\1\3\0005\2\5\0005\3\4\0=\3\6\0025\3\a\0005\4\b\0=\4\t\3=\3\n\2=\2\v\0015\2\f\0005\3\r\0005\4\14\0=\4\t\3=\3\n\2=\2\15\0015\2\18\0005\3\16\0005\4\17\0=\4\t\3=\3\n\2=\2\19\1B\0\2\0016\0\20\0009\0\21\0'\1\22\0B\0\2\1K\0\1\0\"nnoremap \\ :NeoTreeReveal<cr>\bcmd\bvim\15git_status\1\0\0\1\0\18\6p\25paste_from_clipboard\6C\15close_node\6d\vdelete\agc\15git_commit\18<2-LeftMouse>\topen\agu\21git_unstage_file\6A\16git_add_all\t<cr>\topen\aga\17git_add_file\6r\vrename\agr\20git_revert_file\6s\16open_vsplit\agg\24git_commit_and_push\6c\22copy_to_clipboard\6R\frefresh\6S\15open_split\6x\21cut_to_clipboard\agp\rgit_push\1\0\1\rposition\nfloat\fbuffers\1\0\r\6p\25paste_from_clipboard\6a\badd\6.\rset_root\6d\vdelete\18<2-LeftMouse>\topen\6s\16open_vsplit\6r\vrename\t<bs>\16navigate_up\6c\22copy_to_clipboard\6R\frefresh\t<cr>\topen\6S\15open_split\6x\21cut_to_clipboard\1\0\1\rposition\tleft\1\0\1\18show_unloaded\2\15filesystem\vwindow\rmappings\1\0\20\6p\25paste_from_clipboard\6a\badd\6C\15close_node\6x\21cut_to_clipboard\18<2-LeftMouse>\topen\6d\vdelete\6I\21toggle_gitignore\6H\18toggle_hidden\t<bs>\16navigate_up\t<cr>\topen\n<c-x>\17clear_filter\6r\vrename\6s\16open_vsplit\6/\23filter_as_you_type\6c\22copy_to_clipboard\6R\frefresh\6.\rset_root\6S\15open_split\6f\21filter_on_submit\abd\18buffer_delete\1\0\2\rposition\tleft\nwidth\3(\ffilters\1\0\2\24follow_current_file\1\27use_libuv_file_watcher\1\1\0\2\22respect_gitignore\2\16show_hidden\1\1\0\3\23popup_border_style\frounded\22enable_git_status\2\23enable_diagnostics\2\nsetup\rneo-tree\frequire\0" },
    loaded = true,
    path = "/home/anindra/.local/share/nvim/site/pack/packer/start/neo-tree.nvim",
    url = "https://github.com/nvim-neo-tree/neo-tree.nvim"
  },
  ["nui.nvim"] = {
    loaded = true,
    path = "/home/anindra/.local/share/nvim/site/pack/packer/start/nui.nvim",
    url = "https://github.com/MunifTanjim/nui.nvim"
  },
  ["nvim-web-devicons"] = {
    loaded = true,
    path = "/home/anindra/.local/share/nvim/site/pack/packer/start/nvim-web-devicons",
    url = "https://github.com/kyazdani42/nvim-web-devicons"
  },
  ["packer.nvim"] = {
    loaded = true,
    path = "/home/anindra/.local/share/nvim/site/pack/packer/start/packer.nvim",
    url = "https://github.com/wbthomason/packer.nvim"
  },
  ["plenary.nvim"] = {
    loaded = true,
    path = "/home/anindra/.local/share/nvim/site/pack/packer/start/plenary.nvim",
    url = "https://github.com/nvim-lua/plenary.nvim"
  },
  ["vscode.nvim"] = {
    loaded = true,
    path = "/home/anindra/.local/share/nvim/site/pack/packer/start/vscode.nvim",
    url = "https://github.com/Mofiqul/vscode.nvim"
  }
}

time([[Defining packer_plugins]], false)
-- Config for: neo-tree.nvim
time([[Config for neo-tree.nvim]], true)
try_loadstring("\27LJ\2\2�\t\0\0\5\0\23\0\0316\0\0\0'\1\1\0B\0\2\0029\0\2\0005\1\3\0005\2\5\0005\3\4\0=\3\6\0025\3\a\0005\4\b\0=\4\t\3=\3\n\2=\2\v\0015\2\f\0005\3\r\0005\4\14\0=\4\t\3=\3\n\2=\2\15\0015\2\18\0005\3\16\0005\4\17\0=\4\t\3=\3\n\2=\2\19\1B\0\2\0016\0\20\0009\0\21\0'\1\22\0B\0\2\1K\0\1\0\"nnoremap \\ :NeoTreeReveal<cr>\bcmd\bvim\15git_status\1\0\0\1\0\18\6p\25paste_from_clipboard\6C\15close_node\6d\vdelete\agc\15git_commit\18<2-LeftMouse>\topen\agu\21git_unstage_file\6A\16git_add_all\t<cr>\topen\aga\17git_add_file\6r\vrename\agr\20git_revert_file\6s\16open_vsplit\agg\24git_commit_and_push\6c\22copy_to_clipboard\6R\frefresh\6S\15open_split\6x\21cut_to_clipboard\agp\rgit_push\1\0\1\rposition\nfloat\fbuffers\1\0\r\6p\25paste_from_clipboard\6a\badd\6.\rset_root\6d\vdelete\18<2-LeftMouse>\topen\6s\16open_vsplit\6r\vrename\t<bs>\16navigate_up\6c\22copy_to_clipboard\6R\frefresh\t<cr>\topen\6S\15open_split\6x\21cut_to_clipboard\1\0\1\rposition\tleft\1\0\1\18show_unloaded\2\15filesystem\vwindow\rmappings\1\0\20\6p\25paste_from_clipboard\6a\badd\6C\15close_node\6x\21cut_to_clipboard\18<2-LeftMouse>\topen\6d\vdelete\6I\21toggle_gitignore\6H\18toggle_hidden\t<bs>\16navigate_up\t<cr>\topen\n<c-x>\17clear_filter\6r\vrename\6s\16open_vsplit\6/\23filter_as_you_type\6c\22copy_to_clipboard\6R\frefresh\6.\rset_root\6S\15open_split\6f\21filter_on_submit\abd\18buffer_delete\1\0\2\rposition\tleft\nwidth\3(\ffilters\1\0\2\24follow_current_file\1\27use_libuv_file_watcher\1\1\0\2\22respect_gitignore\2\16show_hidden\1\1\0\3\23popup_border_style\frounded\22enable_git_status\2\23enable_diagnostics\2\nsetup\rneo-tree\frequire\0", "config", "neo-tree.nvim")
time([[Config for neo-tree.nvim]], false)
if should_profile then save_profiles() end

end)

if not no_errors then
  vim.api.nvim_command('echohl ErrorMsg | echom "Error in packer_compiled: '..error_msg..'" | echom "Please check your config for correctness" | echohl None')
end
